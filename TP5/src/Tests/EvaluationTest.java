package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import Classes.Evaluation;
import Exceptions.InvalidGradeFormatException;

public class EvaluationTest {

    private static Evaluation eval1 ;
    private static Evaluation eval2 ;

    @Test
    public void testGetGradesSAE() {
        assertEquals("ABBC", eval2.getGradesSAE()) ;
        assertEquals("", eval1.getGradesSAE()) ;
    }

    @Test
    public void testSetGradesSAE() {
        try {
            eval1.setGradesSAE("kys") ;
            eval1.setGradesSAE("a") ;
            fail() ;
        }
        catch (InvalidGradeFormatException e) {
        }

        try {
            eval1.setGradesSAE("A") ;
        }

        catch (InvalidGradeFormatException e) {
            fail() ;
        }
    }
    
    @Test
    public void testAddGrade() {

        String message = "Error not supposed to be threw : ";
        Evaluation eval = null;

        try {
            eval = new Evaluation("AABBCBA");
        }catch(InvalidGradeFormatException e) {
            e.printStackTrace();
        }
        
        try {
            eval.addGrade('D');
        }catch(InvalidGradeFormatException e) {
            System.err.println(message + "Correctly added value");
        }

        try {
            eval.addGrade('d');
            fail(message + "Letter in lower case");
        }catch(InvalidGradeFormatException e) {
            e.printStackTrace();
        }

        try {
            eval.addGrade('E');
            fail(message + "Letter out of range");
        }catch(InvalidGradeFormatException e) {
            e.printStackTrace();
        }

        try {
            eval.addGrade(' ');
            fail(message + "Special character added");
        }catch(InvalidGradeFormatException e) {

        }
    }

    @Test
    public void testRemoveTwoLowestGrades() {
        Evaluation eval = null;
        try {
            eval = new Evaluation("AABC");
        }catch(InvalidGradeFormatException e) {
            e.printStackTrace();
        }

        eval.removeTwoLowestGrades();

        assertTrue(eval.getGradesSAE().equals("AA"));
    } 

    @Test
    public void testToString() {
        eval1 = new Evaluation() ;
        try {
            eval2 = new Evaluation("CAC");
        }catch(InvalidGradeFormatException e) {
            e.printStackTrace();
        }
        assertNotEquals("", eval1.toString()) ;
        assertEquals("CAC", eval2.toString()) ;
    }

    @Test
    public void testLongestASequence() {
        try {
            eval2 = new Evaluation("AABCAAABABCAAAABAA") ;
        }catch(InvalidGradeFormatException e) {
            e.printStackTrace();
        }
        assertEquals(0, eval1.longestASequence()) ;
        assertEquals(4, eval2.longestASequence()) ;
    }

    @Test
    public void testMostCommonGrade() {
        try {
            eval2 = new Evaluation("ABBCACC") ;
        }catch(InvalidGradeFormatException e) {
            e.printStackTrace();
        }
        assertNull(eval1.mostCommonGrade()) ;
        assertEquals((Character)'C', eval2.mostCommonGrade()) ;
    }

    public void testGetMedianGrade() {
        try {
            eval2 = new Evaluation("CAAC") ;
        }catch(InvalidGradeFormatException e) {
            e.printStackTrace();
        }
        assertNull(eval1.getMedianGrade()) ;
        assertEquals((Character)'B', eval2.getMedianGrade()) ;
        try {
            eval2 = new Evaluation("BA") ;
        }catch(InvalidGradeFormatException e) {
            e.printStackTrace();
        }
        assertEquals((Character)'A', eval2.getMedianGrade()) ;
    }

}
