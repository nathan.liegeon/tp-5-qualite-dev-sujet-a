package Classes;

import Exceptions.InvalidGradeFormatException;

public class Evaluation {
    
    private String gradesSAE ;

    /**
     * Initializes an Evalution with an empty grade
     */
    public Evaluation() {
        // TODO Constructor with an empty grade
    }

    /**
     * Initializes an Evaluation with a grade
     * Uses the setGradesSAE method and if it throws an Exception, catches it
     * @param gradesSAE
     * @throws InvalidGradeFormatException
     */
    public Evaluation(String gradesSAE) throws InvalidGradeFormatException {
        // TODO Constructor with a grade, has to throw an Exception
    }

    /**
     * Returns the value of the field gradesSAE
     * @return
     */
    public String getGradesSAE() {
        // TODO 
    }

    /**
     * Changes the value of the field gradesSAE and throws an Exception if it's invalid,
     * meaning it contains characters other than 'A', 'B' or 'C'
     * @param gradesSAE
     * @throws InvalidGradeFormatException
     */
    public void setGradesSAE(String gradesSAE) throws InvalidGradeFormatException {
        // TODO
    }

    /**
     * Returns the gradeSAE field, if it is empty, returns a special message
     */
    public String toString() {
        // TODO
    }

    /* /**
     * Returns the best grade
     * @return (String) - The best grade, like examble "A" or "B", if the String is empty, then return "null"
     *
    public String getBestGrade() {

    } */

    /**
     * Adds a grade to the gradesSAE attributes, after the grades that are already written.
     * @param string (char) - The new grade to add
     * @throws InvalidGradeFormatException Threw if the newGrade is not 'A', 'B', 'C' or 'D'
     */
    public void addGrade(char grade) throws InvalidGradeFormatException {

    }

    /**
     * Removes the two lowest grades.
     */
    public void removeTwoLowestGrades() {

    }

    /**
     * Returns the length of the longest consecutive sequence of As in the grade
     * @return
     */
    public int longestASequence() {
        // TODO
    }

    /**
     * Returns the most common grade the student got
     * In case of a tie, return the best grade between the 2, 
     * if the string containing the grades is null, return null
     * @return 
     */
    public Character mostCommonGrade() {
        // TODO
    }

    /**
     * Returns the median grade, if the median falls between two different grades, if they are adjacent 
     * (A and B or B and C), return the highest, else, (so in case of A and C), return the middle value (here B)
     * @returns null if the string is empty, 
     */
    public Character getMedianGrade() {
        //TODO
    }



}
