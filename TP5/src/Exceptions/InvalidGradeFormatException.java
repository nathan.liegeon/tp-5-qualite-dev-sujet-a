package Exceptions;

/**
 * Exception thrown when a grade contains characters other than 'A', 'B' or 'C'
 */
public class InvalidGradeFormatException extends Exception {
    InvalidGradeFormatException(String grade) {
        super(grade + " Is an invalid format, it should only contain the Characters 'A' 'B' or 'C") ;
    }
}
